Author : Philippe 'iamphinson' NDIAYE
email  : phndiaye@gmail.com

This script can be used to get a documentation page by langage from the CLI
Or directly get the result of a search from the language doc.

Supported languages : python, cpp , php 

Usage  :	python getdoc.py [language]
			Example: python getdoc.py cpp

Usage :		python getdoc.py [language] [search subject]
		 	Example : python getdoc.py cpp operator
