#! /usr/bin/python 

# getdoc.py : Get documentation base page of a language from the command line 
# Written by Philippe 'iamphinson' Ndiaye (phndiaye@gmail.com / www.iamphinson.com)

import sys
import os 
import webbrowser

doc_url = None 
search_url = None
# Implemented languages 
options = [
	'python',
	'cpp',
	'php'
]

def get_base_doc(language):
	global doc_url , options

	# Test if the given language is implemented or not. 
	# If so, the corresponding url is opened in the web browser
	if language in options:
		if (language == 'python'):
			doc_url = 'http://www.python.org/doc/'			
		elif (language == 'cpp'):
			doc_url = 'http://www.cplusplus.com/doc/'
		elif (language == 'php'):
			doc_url = 'http://www.php.net/docs.php'
		else:
			pass

		webbrowser.open_new_tab(doc_url)
	else:
		print("Language not implemented yet. You can add it to the code.")
		usage()

def getSearchResult(language,searching):
	
	global search_url

	if (language == 'python'):
		search_url = 'http://docs.python.org/3/search.html?q='+str(searching)		
	elif (language == 'cpp'):
		search_url = 'http://www.cplusplus.com/search.do?q='+str(searching)
	elif (language == 'php'):
		search_url = 'http://fr2.php.net/manual-lookup.php?pattern='+str(searching)+'s&lang=fr&scope=quickref'
	else:
		pass
	
	webbrowser.open_new_tab(search_url)

def usage():
	print '\nUsage : \t python', os.path.basename(sys.argv[0]), '[language]'
	print '\t\t Example : python', os.path.basename(sys.argv[0]), 'cpp\n'
	print 'Usage : \t python', os.path.basename(sys.argv[0]), '[language] [search subject]'
	print '\t\t Example : python', os.path.basename(sys.argv[0]), 'cpp operator\n'
	print 'Supported languages : ', options, '\n'
	raise SystemExit	 # Exit the program withoud printing the exception raised on screen

def main():

	# if only the language is given as parameter, only the doc page is returned 
	if (len(sys.argv) == 2):
		language = sys.argv[1]
		get_base_doc(language)
	# if the language is given with another parameter, the last is searched on the language doc searching form 
	elif(len(sys.argv) == 3):
		language = sys.argv[1]
		searching = sys.argv[2]
		getSearchResult(language,searching)
	
	# if not one of the other cases is matched, the usage is printed on screen 
	else :
		usage()
	
if __name__ == '__main__':
	main()